# COMP250 Assignment 3 Tester

Inspired by Jacob Tian's BananaTester (who was himself inspired by Zhangyuan's AnotherA3Tester ), I've decided to release my own tester which is strongly based on the official tester.

The main feature's are:


*  New test method: testValidity checks if your tree is binary, if your leaves are truly leaves and your branches are truly binary branches
* Constructor testing returns an average across an arbitrary number of runs
* Testing of different methods can be turned on and off at will, so you won't be blasted with print statements you don't care about
* Testing paramters are declared at the top for easy access


Please keep in mind this tester wasn't designed to be made public, so it may not be as polished as it should be. If you find any errors, or want to update the code, please let me know!